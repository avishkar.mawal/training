﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab13
{
    class Department
    {
        public int dep_Id { get; set; }
        public string dep_Name { get; set; }
        public string dep_Loc { get; set; }

        
        public override string ToString()
        {
            return string.Format("Department id: {0}\t Department Name:{1}\t Department Location:{2}", dep_Id, dep_Name, dep_Loc);
        }
    
    }
}
