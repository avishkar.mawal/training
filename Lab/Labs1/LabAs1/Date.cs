﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabAs1
{
    class Date
    {
        public int mDay { get; set; }
        public int mMonth { get; set; }
        public int mYear { get; set; }

        public Date(int d, int m, int y)
        {
            mDay = d;
            mMonth = m;
            mYear = y;
        }

        public void printdate()
        {
            Console.WriteLine(mDay+"-"+mMonth+"-"+mYear);
        }
    }
}
