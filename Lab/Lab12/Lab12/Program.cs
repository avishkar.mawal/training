﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Collections;
namespace CollectionApp
{
    class Program
    {
        private static int[] intValues = { 1,13, 4, 5, 6 };
        private static double[] doubleValues = { 8.4, 9.3, 0.2, 7.9, 3.4 };
        private static int[] intValuesCopy;


        static void Main(string[] args)
        {
            //1.Array
            intValuesCopy = new int[intValues.Length];
            Console.WriteLine("Initial array values\n");
            PrintArrays();

            //sort doubleValues... static method of aaray class
            Array.Sort(doubleValues);
            Array.Sort(intValues);
            //copy values from intvalues to intValuescopy array

            Array.Copy(intValues, intValuesCopy, intValues.Length);

            Console.WriteLine("\n Array values after sort and copy");
            PrintArrays();

            // search 5 in intValues array
            int result = Array.BinarySearch(intValues, 5);

            if (result >= 0)
            {
                Console.WriteLine("5 found at element {0} in intValue", result);
            }
            else
            {
                Console.WriteLine("5 is not found in intValues:");
            }

            // search 8783 in intValues

            int result1 = Array.BinarySearch(intValues, 8783);
            if (result1 >= 0)
            {
                Console.WriteLine("8783 found at {0} ", result1);


            }
            else
            {
                Console.WriteLine("8783 is not found in intValues:");
            }
            Console.ReadLine();

        }

        private static void PrintArrays()
        {
            Console.WriteLine("doublevalues..");
            IEnumerator enumerator = doubleValues.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current + " ");

            }
            Console.WriteLine("\n intValeus:");


            enumerator = intValues.GetEnumerator();
            while (enumerator.MoveNext())
            {
                Console.WriteLine(enumerator.Current + "");
            }
            Console.WriteLine("\n intValuesCopy");
            foreach (var element in intValuesCopy)
            {
                Console.WriteLine(element + " ");
            }

        }
    }
}
