﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab6
{
  public  class employee
    {

        public int emp_id { get; set; }
        public string emp_name { get; set; }
        public int design { get; set; }
        public double basic_sal { get; set; }
        public double HRA { get; set; }
        public double DA { get; set; }
        public double PF { get; set; }
        public double GS { get; set; }
        public double NetSal { get; set; }
        public void dis()
        {
            HRA = .08 * basic_sal;
            PF = .12 * basic_sal;
            GS = basic_sal + HRA;
            NetSal = GS - PF;
        }

        public override string ToString()
        {
            return string.Format("Emp_id:{0}\nEmp_Name:{1}\nEmp_Designation:{2}\nBasic Salary:{3}\nHRA:{4}\nDA:{5}\nProvidend Fund :{6}" +
                                "\nGross Salary : {7}\nNet Salary:{8}",emp_id,emp_name,design,basic_sal,HRA,DA,PF,GS,NetSal); 
        }
        

    }
}
