﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
namespace ThreadApp1
{
    class Printer
    {
         
        public void Print(object message)
        {
            //aquire a lock
            Monitor.Enter(this);
            try
            {
                //this block of code is critical
                Console.Write("***" + message);
                Thread.Sleep(5000);
                Console.WriteLine("****");

            }
            catch (Exception)
            {

                //Exception handling goes here
            }
            finally
            {
                Monitor.Exit(this);
            }
                
        }
    }
}
