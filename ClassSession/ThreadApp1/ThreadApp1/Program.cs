﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ThreadApp1
{
    class Program
    {
        static void Main(string[] args)
        {


            Tictoc tt = new Tictoc();
            MyThread t1 = new MyThread("Tick ", tt);
            MyThread t2 = new MyThread("Tock", tt);

            t1.t.Join();
            t2.t.Join();

            //Printer p = new Printer();
            ////used to pass parameter to method

            //ParameterizedThreadStart ps = p.Print;
            //Thread t1 = new Thread(ps);
            //Thread t2 = new Thread(ps);
            //Thread t3 = new Thread(ps);
            //t1.Start("Welcome..");
            //t2.Start("To");
            //t3.Start("tripstack");

            //t1.Join();
            //t2.Join();
            //t3.Join();

            ////Thread t = new Thread(new ThreadStart(Test.DoSomething));
            //t.Start();
            //Thread.Sleep(1000);
            //t.Abort();
            //t.Start();
            //t.Join();
            //Console.WriteLine("Done....");


            //Console.WriteLine("Main() on thread no :{0} ",Thread.CurrentThread.ManagedThreadId);
            //Printer a = new Printer('.', 10,"a");
            //Printer b = new Printer('*', 50,"b");

            //Thread t = new Thread(new ThreadStart(a.Print)); // create new stack
            //Thread t1 = new Thread(new ThreadStart(b.Print));
            //Console.WriteLine("Priority of t :{0}",t.Priority);
            //Console.WriteLine("Priority of T1 is{0}",t1.Priority);
            //t.Start();
            //t1.Start();
            //Console.WriteLine("Main() is waiting for child to finish..");
            //t.Join();
            //t1.Join();

            //Console.WriteLine("Main() completed");
            Console.ReadLine();

        }
    }
}
