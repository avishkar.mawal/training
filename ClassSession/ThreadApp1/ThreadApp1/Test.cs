﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp1
{
    class Test
    {
        public static void DoSomething()
        {
            try
            {

                try
                {
                    try
                    {

                    }
                    catch (ThreadAbortException)
                    {

                        Console.WriteLine("---Inner aborted.....");
                    }
                }
                catch (ThreadAbortException)
                {
                    Console.WriteLine("---Outer aborted....");
                }

            }
            finally
            {

                Console.WriteLine("--Finally ...");
            }
        }
    }
}
