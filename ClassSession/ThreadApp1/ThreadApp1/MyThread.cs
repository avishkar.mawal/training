﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadApp1
{
    class MyThread
    {
        public Thread     t;
        Tictoc tt;
        public MyThread(string name, Tictoc tiObj)
        {
            t = new Thread(this.run);
            this.tt = tiObj;
            t.Name = name;
            t.Start();

        }

        public void run()
        {
            if (t.Name == "Tick")
            {
                for (int i = 0; i < 5; i++)
                {
                    tt.Tick(true);
                }
            }
        }
    }
}
